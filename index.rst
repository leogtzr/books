.. Sphinx Example Project documentation master file, created by
   sphinx-quickstart on Tue Oct 25 09:18:20 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Books in my library
==================================================

Contents:

.. toctree::
   :maxdepth: 2



Indices
==================

* :ref:`genindex`
* :ref:`search`

.. Books
.. ==================

.. * https://gitlab.com/pages/sphinx

==================
My physical books:
==================
A. Aberastury, M. Knobel (1)
----------------------------
**La adolescencia normal - Un enfoque psicoanalítico**


Aaron T. Beck (1)
-----------------
**Prisioneros del Odio - Las bases de la ira, la hostilidad y la violencia**


Adrian Furnham (1)
------------------
**50 Cosas que hay que saber sobre psicología**


Akiyuki Nosaka (1)
------------------
**La tumba de las luciérnagas / Las algas americanas**


Alan Willett (1)
----------------
**Leading the unleadable - How to Manage Mavericks, Cynics, Divas, and Other Difficult People**


Albert Camus (1)
----------------
**El extranjero**


Alberto Constante (2)
---------------------
**La metáfora de las cosas - (Nietzsche, Heidegger, Rilke, Freud)**

**El asombro ante el mundo (o el infinito silencio)**


Alessandro Baricco (1)
----------------------
**Seda**


Alfonso Escárcega (1)
----------------------
**La mera mera - Luz Corral de Villa**
	Prólogo de Jorge Aguilar Mora


Alfredo Espinosa (1)
--------------------
**El reino en ruinas**


American Psychiatric Association (1)
------------------------------------
**DSM-5 - Manual Diagnóstico y Estadístico de los Trastornos Mentales**


Ana Frank (1)
-------------
**Diario de Ana Frank**


Andrés Vela Pérez (1)
-----------------------
**Bodrio**


Anna Freud (1)
--------------
**El yo y los mecanismos de defensa**


Anónimo (1)
------------
**El Lazarillo de Tormes**


August Strindberg (1)
---------------------
**Inferno**


Augusto Cury (1)
----------------
**Ansiedad - Cómo enfrentar el mal del siglo**
	El síndrome del Pensamiento Acelerado: cómo y por qué la humanidad enfermó colectivamente


Bobbie Ann Mason (1)
--------------------
**Shiloh**


Bram Stoker (1)
---------------
**El entierro de las ratas y otros relatos de horror**
	Prólogo con vida y obra del autor


Brian B. Greene (1)
-------------------
**El universo elegante - Supercuerdas, dimensiones ocultas y la búsqueda de una teoría final**


Brian W. Kernighan (1)
----------------------
**El Lenguaje de Programación C**


Brian Weiss (1)
---------------
**Muchas vidas, muchos maestros**


Byung-Chul Han (10)
-------------------
**La sociedad del cansancio**

**La agonía del Eros**

**La expulsión de lo distinto**

**La sociedad de la transparencia**

**Psicopolítica**

**Sobre el poder**

**La salvación de lo bello**

**El aroma del tiempo**

**Topología de la violencia**

**En el enjambre**


Carlos Fuentes (3)
------------------
**Aura**

**Agua Quemada**

**Cantar De Ciegos**


Carlos Ruiz Zafón (1)
----------------------
**Marina**


Chad Fowler (1)
---------------
**The Passionate Programmer - Creating a Remarkable Career in Software Development**


Christophe André (1)
---------------------
**Psicología del miedo**


Colin Duriez (1)_.
-----------------------
**Tolkien y El Señor de Los Anillos**


César Francisco Pacheco Loya (1)
---------------------------------
**Encuentro con un medio desconocido**


Daniel Goleman (1)
------------------
**Emotional Intelligence**
	The tenth anniversary edition


Daniel Tubau (1)
----------------
**No tan elemental - Cómo ser Sherlock Holmes**


Dante Alighieri (1)
-------------------
**Divina Comedia**
	Edición de Giorgio Petrocchi y Luis Martínez de Merlo


David Sumpter (1)
-----------------
**Futbol y matemáticas - Aventuras matemáticas del deporte rey**


David Toscana (1)
-----------------
**El último lector**


Deborah Peterson (1)
--------------------
**PNL - Conoce cómo te puede apoyar**


Donna M. Genett, Ph.D. (1)
--------------------------
**If you Want It Done Right, You Don't Have to Do It Yourself!**


Douglas R. Hofstadter (1)
-------------------------
**Gödel, Escher, Bach - Un eterno y grácil bucle**


Edgar Allan Poe (2)
-------------------
**Cuentos 1**

**Cuentos**


Edward Burman (1)
-----------------
**Los secretos de la Inquisición - Historia y legado del Santo Oficio, desde Inocencio III a Juan Pablo II**


Emmanuel Carrère (1)
---------------------
**El Reino**


Erich Fromm (1)
---------------
**El dogma de Cristo**


Esther Cohen (1)
----------------
**Con el diablo en el cuerpo**


Federico Nietzsche (1)
----------------------
**Así hablaba Zaratustra**


Felipe Sen Montero, Ángel Sánchez Rodríguez (1)
--------------------------------------------------
**Cuentos Clásicos del Antiguo Egipto**


Fernando Garcés Blázquez (1)
------------------------------
**La historia del mundo - Sin los trozos aburridos**


Francisco Cruz, Marco A. Durán (1)
-----------------------------------
**Los Depredadores - La historia oscura del presidencialismo**


Francisco Manuel Saurí Mercader (1)
------------------------------------
**Russell - Solo la lógica permite al hombre pensar con claridad**


Friedrich Nietzche (3)
----------------------
**Más allá del bien y del mal**

**Humano, demasiado humano**

**El Anticristo**


Gabriel García Márquez (1)
----------------------------
**Memoria de mis putas tristes**


Gayle Laakmann MCDowell (1)
---------------------------
**Cracking the Code Interview**


George Orwell (2)
-----------------
**Rebelión en la granja**

**1984**


George R. R. Martin (1)
-----------------------
**Juego de Tronos - Canción de hielo y fuego**


H. P. Lovecraft (2)
-------------------
**Ciclo Onírico: Visiones de Horror y Muerte, Tomo I**
	Tomo I, Narrativa Completa, Prólogo Alberto Chimal

**Ciclo Mítico: Los Antiguos Dioses, Tomo II**
	Tomo II, Narrativa Completa, Prólogo: Ramón López Castro


Hellmuth G. Dahms (1)
---------------------
**La segunda Guerra Mundial**


Henry S. Warren, JR. (1)
------------------------
**Hacker's Delight - Second Edition**


Hermann Hesse (2)
-----------------
**Siddharta**

**Bajo la Rueda**


Homero (1)
----------
**La Odisea**


Immanuel Kant (1)
-----------------
**Crítica de la razón pura**


Irvin D. Yalom (1)
------------------
**El día que Nietzsche lloró**


J. J. Benítez (1)
-----------------
**La rebelión de Lucifer**

.. _tolkien:


`J. R. R. Tolkien (37)`
-----------------------
:ref:`tolkien`

**El señor de los anillos - La comunidad del anillo I**
	Edición minotauro - 2002

**El señor de los anillos - La Dos Torres**
	Edición Minotauro - 2002

**El señor de los anillos - El Retorno del Rey**
	Edición minotauro - 2002

**La Caída de Arturo**
	Editado por Christopher Tolkien

**Cuentos Inconclusos I - La Primera Edad**
	Ediciones Minotauro - 1988

**Cuentos Inconclusos II - La Segunda Edad**
	Ediciones Minotauro - 1989

**El Silmarillion**
	Ediciones Minotauro - 2001, debolsillo

**El Silmarillion**
	booklet - 2012

**Los Hijos de Húrin**
	booklet - 2011

**Cuentos Desde El Reino Peligroso**
	Booklet - 2012, Ilustrado por Alan Lee

**El Hobbit**
	booklet - 2013

**El señor de los anillos - Tomo I - La Comunidad Del Anillo**
	Ediciones Minotauro - 1991

**El señor de los anillos - Tomo II - Las Dos Torres**
	Ediciones Minotauro - 1991

**El señor de los anillos - Tomo III - El Retorno Del Rey**
	Ediciones Minotauro - 1991

**El Señor de Los Anillos I - La Comunidad Del Anillo - Primera Parte**
	Biblioteca Tolkien - minotauro

**El Señor de Los Anillos III - El Retorno Del Rey**
	Biblioteca Tolkien - minotauro

**El Señor de Los Anillos II - Las Dos Torres**
	Biblioteca Tolkien - minotauro

**El Señor de Los Anillos I - La Comunidad Del Anillo - Segunda Parte**
	Biblioteca Tolkien - minotauro

**J. R. R. Tolkien - Señor De La Tierra Media**
	Biblioteca Tolkien (2012) - minotauro, Edición de Joseph Pearce

**Guía Completa De La Tierra Media (H-Z)**
	Robert Foster, Biblioteca Tolkien (2002) - minotauro

**La Historia De El Señor De Los Anillos - La Traición De Isengard**
	Christopher Tolkien, Biblioteca Tolkien (2002) - minotauro

**Guía Completa De La Tierra Media (A-G)**
	Robert Foster, Biblioteca Tolkien (2002) - minotauro

**El Camino A La Tierra Media**
	T.A. Shippey, Biblioteca Tolkien - minotauro

**Historia De La Tierra Media - La Formación De La Tierra Media**
	Christopher Tolkien, Biblioteca Tolkien (2002) - minotauro

**La Historia De El Señor De Los Anillos - La Guerra Del Anillo**
	Christopher Tolkien, Biblioteca Tolkien (2002) - minotauro

**El Señor De Los Anillos - Apéndices**
	Biblioteca Tolkien (2002) - minotauro

**Historia De La Tierra Media - Las Baladas de Beleriand**
	Christopher Tolkien, Biblioteca Tolkien (2002) - minotauro

**La Historia De El Señor De Los Anillos - El Fin De La Tercera Edad**
	Christopher Tolkien, Biblioteca Tolkien (2002) - minotauro, repetido.

**Historia De La Tierra Media - El Anillo De Morgoth**
	Christopher Tolkien, Biblioteca Tolkien (2002) - minotauro, repetido.

**Árbol y Hoja Y El Poema Mitopoeia**
	Con una introducción de Christopher Tolkien, Biblioteca Tolkien, 2002

**Egidio,el granjero de Ham & El herrero de Wootton Mayor**
	Ediciones Minotauro - 2013

**Hoja de Niggle & Las aventuras de Tom Bombadil**
	Ediciones Minotauro - 2013

**El Hobbit**
	Ediciones Minotauro - 2013

**Roverandom**
	Ediciones Minotauro - 2013

**Las aventuras de Tom Bombadil**
	Ilustrado por Pauline Baynes

**Beowulf - Traducción y Comentario**
	Editado por Christopher Tolkien

**La historia de Kullervo**
	Editado por Verlyn Flieger


Jacques Lacan (4)
-----------------
**Aun**

**Las Psicosis**

**Escritos 1**

**Escritos 2**


Jacques-Alain Miller (1)
------------------------
**El ultimísimo Lacan**


Jalil Gibran (1)
----------------
**El loco, El vagabundo**


James D. Murphy (1)
-------------------
**Flawless Execution**


James Joyce (1)
---------------
**Ulises**
	Spanish version.


James Redfield (1)
------------------
**The Celestine Prophecy**


Jay Fields (1)
--------------
**Working Effectively With Unit Tests**


Jean Paul Sartre (2)
--------------------
**El existencialismo es un humanismo**

**La náusea**


Joanne Baker (1)
----------------
**50 Cosas que hay que saber sobre Física**


Jocko Willink (1)
-----------------
**Extreme Ownership: How U.S. Navy Seals Lead and Win**


John Gray (1)
-------------
**Misa Negra - La rebelión apocalíptica y la muerte de la utopía**


Jonathan Rasmusson (1)
----------------------
**The Agile Samurai - How Agile Masters Deliver Great Software**


Joshua Bloch (1)
----------------
**Effective Java - Second Edition**


Juan Rulfo (1)
--------------
**Pedro Páramo**


Judi James (1)
--------------
**La biblia del lenguaje corporal**


Karn Slaughter (1)
------------------
**La buena hija**


Kathy Sierra, Bert Bates (1)
----------------------------
**OCA/OCP Java SE 7 Programmer I & II Study Guide (Exams 1Z0-803 & IZ0-804)**
	Complete Exam Preparation


Kazuo Ishiguro (1)
------------------
**Un artista del mundo flotante**


Lewis Carroll (2)
-----------------
**El juego de la lógica**

**Alicia a través del espejo, Alicia en el país de las maravillas**


Lin Carter (1)
--------------
**Tolkien El Origen De El Señor De Los Anillos**


Luis Herrera-Lasso (1)
----------------------
**Fenomenología de la violencia**


Malcolm Gladwell (1)
--------------------
**Outliers**


Marcelo Hernán Ruiz (1)
------------------------
**Programación C - El lenguaje fundamental que todo programador debe dominar**
	Manuales USERS


Mario Livio (1)
---------------
**La ecuación jamás resuelta**


Mark Twain (1)
--------------
**El Príncipe y el Mendigo**


Marqués De Sade (1)
--------------------
**Los 120 días de Sodoma**


Michael Burleigh (1)
--------------------
**El tercer Reich - Una nueva historia**


Michael Foucalt (1)
-------------------
**Historia de la sexualidad I**


Michel Foucalt (1)
------------------
**Historia de la sexualidad - II. el uso de los placeres**


Natsume Sose (1)
----------------
**Yo, el gato**
	#cat


Nicolás Maquiavelo (1)
-----------------------
**El príncipe**


Norman Cameron (1)
------------------
**Desarrollo de la personalidad y psicopatología - Un enfoque dinámico**


Octavio Paz (3)
---------------
**El laberinto de la soledad, Postdata, Vuelta a "El laberinto de la soledad"**

**Las palabras y los días - Una antología introductoria**

**Lo mejor de Octavio Paz I**
	Poesía


Oliver Bowden (1)
-----------------
**Assasin's Creed**


Patrick Rothfuss (2)
--------------------
**El temor de un hombre sabio**

**La música del silencio**


Platón (2)
-----------
**Diálogos**

**La República**


Público (3)
------------
**Miguel Ángel - Grandes maestros de la pintura**

**Leonardo - Grandes maestros de la pintura**

**Rafael - Grandes maestros de la pintura**


Renato D. Alarcón, Guido Mazzotti, Humberto Nicolini (1)
---------------------------------------------------------
**Psiquiatría**


Ricardo Raphael (1)
-------------------
**Mirreynato - La otra desigualdad**


Richard Matherson (1)
---------------------
**Soy Leyenda**


Robert C. Martin (1)
--------------------
**Clean Code - A Handbook of Agile Software Craftmanship**
	Robert C. Martin series


Roberto Bolaño (1)
-------------------
**2666**


Rogelio Diaz-Guerrero (1)
-------------------------
**Psicología del Mexicano 2**


Rogelio Díaz-Guerrero (1)
--------------------------
**Psicología del Mexicano**


Roger Bartra (1)
----------------
**Anatomía del mexicano**


Rubem Fonseca (1)
-----------------
**Secreciones, excreciones y desatinos**


S G Ganesh And Tushar Sharma (1)
--------------------------------
**Oracle Certified Professional Java SE 7 Programmer Exams 1Z0-804 and 1Z0-805**


Samuel P. Huntington (1)
------------------------
**El choque de civilizaciones**


Sandro Mancuso (1)
------------------
**The Software Craftsman**
	Robert C. Martin Series


Schopenhauer (1)
----------------
**Los dolores del mundo**


Sigmund Freud (4)
-----------------
**El hombre de las Ratas y la neurosis obsesiva**

**Tres ensayos sobre teoría sexual**

**Nuevas aportaciones a la psicoanálisis**

**Psicología de las masas**


Silvia Fendrik, Alfredo Jerusalinsky (1)
----------------------------------------
**El libro negro de la psicopatología contemporánea**


Slawomir Mrozek (1)
-------------------
**La vida difícil**


Stephen Hawking (1)
-------------------
**Agujeros Negros**


Stephen King (1)
----------------
**Carrie**


Sydney Finkelstein (1)
----------------------
**Superbosses**


Thomas H. Cormen (1)
--------------------
**Algorithms Unlocked**


Thomas H. Cormen, Charles E. Leiserson, Ronald L. Rivest, Clifford Stein (1)
----------------------------------------------------------------------------
**Introduction to Algorithms - Second Edition**


Tim Ferris (1)
--------------
**Tools of Titans**
	The tactics, routines, and habits of billionaires, icons, and world-class performers.


Tirso De Molina (1)
-------------------
**El condenado por desconfiado**


Umberto Eco (2)
---------------
**La estructura ausente - Introducción a la semiótica**

**Tratado de Semiótica general**


Varios (1)
----------
**Homenaje a Tolkien - 19 Relatos Fantásticos**
	Selección de Martin H. Greenberg


Vladimir Nabokov (1)
--------------------
**Lolita**


William Shakespeare (1)
-----------------------
**Sonetos**


Wolfgang Benz (1)
-----------------
**El tercer Reich - 101 preguntas fundamentales**


Yann Martel (1)
---------------
**Beatriz y Virgilio**


Yasunari Kawabata (1)
---------------------
**Lo bello y lo triste, test tolkien_.**


Yegor Bugayenko (1)
-------------------
**Elegant Objects**


Élisabeth Roudinesco (1)
-------------------------
**¿Por qué el psicoanálisis?**


Étienne Klein (1)
------------------
**¿Existe el tiempo?**


Óscar Wilde (1)
----------------
**El retrato de Dorian Gray**


