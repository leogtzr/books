package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sort"
	"strings"
)

const (
	bookRSTextFormat = "%s (%d)"
)

// Book ...
type Book struct {
	Name   string `json:"name"`
	Author string `json:"author"`
	Notes  string `json:"notes"`
}

// Title ...
func (b Book) Title() string {
	var str strings.Builder
	str.WriteString("### '")
	str.WriteString(b.Name)
	str.WriteString("'")
	if len(b.Notes) > 0 {
		str.WriteString(" - ")
		str.WriteString(b.Notes)
	}
	return str.String()
}

// RSText ...
func (b Book) RSText() string {
	var str strings.Builder

	str.WriteString("**")
	str.WriteString(strings.TrimSpace(b.Name))
	str.WriteString("**")
	str.WriteString("\n")

	if len(b.Notes) > 0 {
		str.WriteString("\t")
		str.WriteString(b.Notes)
		str.WriteString("\n")
	}
	return str.String()
}

// ByAuthor ...
type ByAuthor []Book

func (b ByAuthor) Len() int {
	return len(b)
}

func (b ByAuthor) Less(i, j int) bool {
	return b[i].Author < b[j].Author
}

func (b ByAuthor) Swap(i, j int) {
	b[i], b[j] = b[j], b[i]
}

func consolidateNameByFields(name string) string {
	var str strings.Builder
	for _, s := range strings.Fields(name) {
		str.WriteString(strings.Title(s))
		str.WriteRune(' ')
	}
	return str.String()
}

func groupBooksByAuthor(books []Book) map[string][]Book {
	var byAuthor = make(map[string][]Book)
	for _, book := range books {
		authorName := strings.TrimSpace(consolidateNameByFields(book.Author))
		if _, ok := byAuthor[authorName]; ok {
			byAuthor[authorName] = append(byAuthor[authorName], Book{Author: authorName, Name: book.Name, Notes: book.Notes})
		} else {
			byAuthor[authorName] = make([]Book, len(byAuthor[authorName]))
			byAuthor[authorName] = append(byAuthor[authorName], Book{Author: authorName, Name: book.Name, Notes: book.Notes})
		}
	}
	return byAuthor
}

func generateDashes(n int) string {
	var str strings.Builder
	for i := 0; i < n; i++ {
		str.WriteString("-")
	}
	return str.String()
}

func main() {
	var books []Book
	res, err := http.Get("http://leobooks.herokuapp.com/api/all")
	if err != nil {
		log.Fatal(err)
	}

	defer res.Body.Close()

	err = json.NewDecoder(res.Body).Decode(&books)
	if err != nil {
		log.Fatal(err)
	}

	var authorNames []string

	authors := groupBooksByAuthor(books)

	for k := range authors {
		authorNames = append(authorNames, k)
	}

	sort.Strings(authorNames)

	for _, k := range authorNames {
		author := strings.TrimSpace(authors[k][0].Author)
		bookTitle := fmt.Sprintf(bookRSTextFormat, author, len(authors[k]))
		fmt.Print(bookTitle)
		fmt.Println()
		fmt.Print(generateDashes(len(bookTitle)))
		fmt.Println()
		for _, book := range authors[k] {
			fmt.Print(book.RSText())
			fmt.Println()
		}
		fmt.Println()
	}
}
